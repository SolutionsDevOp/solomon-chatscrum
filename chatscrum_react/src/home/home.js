import React from 'react';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import { faCircle } from '@fortawesome/free-solid-svg-icons';
import './home.css';
import { Link } from 'react-router-dom';
import { useNavigate } from 'react-router-dom';
import { useEffect } from 'react';

function Home() {
  //This is used to control which page to go to
  const history = useNavigate();

  //Initialization of the page
  useEffect(() => {
    if (sessionStorage.getItem('token')) {
      // assuming you have a reference to the router history object
      history('/scrumboard/' + sessionStorage.getItem('project_id'));
    }
  }, []);

  return (
<>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous"></link>
<link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet"></link>
    <body>
    <div className="conts" style={{background : 'url(https://res.cloudinary.com/ros4eva/image/upload/v1574762212/back_opckeg.png) left no-repeat, #26A69A', background_size : '1000px', paddingBottom : '0px'}}>
  <nav className="mb-1 navbar navbar-expand-lg navbar-dark navc navbar-style">
    <div className="container">
      <a className="navbar-brand" href="home"><img
          src="https://res.cloudinary.com/ros4eva/image/upload/v1574765308/Rectangle_3_btfp6e.png"></img></a>
      <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-4"
        aria-controls="navbarSupportedContent-4" aria-expanded="false" aria-label="Toggle navigation">
        <span className="navbar-toggler-icon"></span>
      </button>
      <div className="collapse navbar-collapse" id="navbarSupportedContent-4">
        <ul className="navbar-nav ml-auto">
          <li className="nav-item active">
            <a className="nav-link" href="login" style={{fontWeight:600, marginRight:'1em', marginTop: '5px'}}>
              <b style={{fontWeight:650}}> LOGIN</b>
              {/* <span className="sr-only">(current)</span> */}
            </a>
          </li>
          <li className="nav-item">
            <Link to="/signup" className="nav-link">
              <button type="button" className="btn btn-signup">SIGN UP</button>
              </Link>
          </li>
        </ul>
      </div>
    </div>

  </nav>


  <div>
    <div className="row wrapper">
      <div className="col col01">
        <div className="homd">
          <img src="https://res.cloudinary.com/ros4eva/image/upload/v1575474757/Rectangle_rn696d_2_620x488_btoffg.png"
            className="img-fluid imgsg"></img>
          <ul style={{marginLeft: "-6em"}}>
            <li><FontAwesomeIcon icon={faCircle} className="circle-icon"/>Scrum Board
            </li>
            <li><FontAwesomeIcon icon={faCircle} className="circle-icon"/>Group Chat
            </li>
            <li><FontAwesomeIcon icon={faCircle} className="circle-icon"/>
              <span style={{color: "#4dcde4"}}>S</span>
              <span style={{color:  "rgb(40, 163, 122)"}}>l</span>
              <span style={{color: "rgb(204, 41, 41)"}}>a</span>
              <span style={{color: "rgb(241, 205, 44)"}}>c</span>
              <span style={{color: "rgb(66, 19, 66)"}}>k</span> Integration
            </li>
          </ul>
          <span className="lst">All in one with <span style={{color: "#4DFFD4"}}>ChatScrum</span></span><br></br>
          <a href="createuser"><button type="button" className="btn homdbtn">SIGN
              UP</button></a>
          <a href="#"><button className="btn dembtn">Try Out Demo!</button></a>

        </div>
      </div>
      <div className="col imgse">

        <img src="https://res.cloudinary.com/ros4eva/image/upload/v1574980298/Rectangle_rn696d.png" className="img-fluid "></img>

      </div>
    </div>
  </div>
</div>
<div>
  <footer className=" footw">
    <div className="footer-copyright py-3 ">
      <div className="float-left copy" style={{color: 'rgba(0, 0, 0, 0.6)'}}>© Chatscrum 2019</div>
      <div className="float-right copy">
        <ul id="menuli">
          <li><a href="/support">Support</a></li>
          <li><a href="/terms">Terms of use</a></li>
        </ul>
      </div>
    </div>
  </footer>
</div>
</body>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</>
  );
}

export default Home;
